######################################################
#   WELCOME TO INSTALLATION SECTION OF ARTIX LINUX   #
######################################################


# First step
  - login with usr & pass `artix`
  - sudo su
  - check lsblk
  - create partition with cfdisk
  - don't forget to make another partition for /$HOME
  - mkfs.fat -F32 /dev/`partition`
  - fatlabel /dev/`partition` BOOT
  - mkfs.ext4 -L ROOT /dev/`partition`
  - mkfs.ext4 -L HOME /dev/`partition`
  - mkswap -L SWAP /dev/`partition`
  - mount /dev/`partition` /mnt
  - mkdir -p /mnt/boot/efi
  - mkdir -p /mnt/home
  - mount /dev/`partition` /mnt/boot/efi
  - mount /dev/`partition`/mnt/home
  - swapon /dev/`partition`
  - check with lsblk
  - `basestrap /mnt base base-devel openrc linux linux-firmware amd-ucode neovim fish`
  - fstabgen -U /mnt >> /mnt/etc/fstab
  - check with `cat /mnt/etc/fstab`
  - artix-chroot /mnt

# Step 2
  - run with fish shell
  - ln -sf /usr/share/zoneinfo/Asia/Jakarta /etc/localtime
  - hwclock --systohc
  - nvim /etc/locale.gen
  - locale-gen
  - nvim /etc/locale.conf
  - nvim /etc/hostname
  - nvim /etc/hosts
    - `
    - 127.0.0.1   localhost
    - :11         localhost
    - 127.0.1.1   hostname.localdomain   hostname
    - `
 - passwd
 - useradd -mG wheel `username`
 - passwd `username`
 - `pacman -S grub efibootmgr openrc networkmanager-openrc network-manager-applet wpa_supplicant dialog os-prober dosfstools linux-headers openssh-openrc git`
 - `grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB`
 - `update-grub`
 - `rc-update add NetworkManager default`
 - `rc-update add sshd default`
 - setting sudo and doas
 - EDITOR=nvim visudo
 - nvim /etc/doas.conf
    - `
    - permit keepenv :wheel
    - permit username as root
    - `
 - restart the system


# Step 3
 - login with username
 - check doas
 - `doas pacman -S artix-archlinux-support && pacman-key --populate archlinux`
 - `doas echo -e "[communitty]\nInclude=/etc/pacman.d/mirrorlist-arch"`
 - `doas pacman -S stow`
 - git clone https://gitlab.com/AnasR7/dotfiles.git
 - stow pacman and paru
 - cd Repository/
 - git clone https://aur.archlinux.org/paru-git.git
 - cd paru-git
 - makepkg -si
 - test paru
 - `paru -S bspwm`
