set runtimepath+=~/.nvim_runtime/settings
source ~/.nvim_runtime/settings/basic.vim
source ~/.nvim_runtime/settings/plugins_config.vim
try
  source ~/.nvim_runtime/settings/my_configs.vim
catch
endtry
