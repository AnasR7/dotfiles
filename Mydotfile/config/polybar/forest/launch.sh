#!/usr/bin/env bash

# Add this script to your wm startup file.

DIR="$HOME/.config/polybar/forest"

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch the bar solo monitor
polybar -q main -c "$DIR"/config.ini &

# Launch polybar dual_monitor
# polybar -q main_monitor -c "$DIR"/config.ini &
# polybar -q secondary_monitor -c "$DIR"/config.ini &
