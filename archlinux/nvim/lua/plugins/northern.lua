return {
  {
    "fcancelinha/northern.nvim",
    branch = "master",
    name = "northern",
    priority = 1000, -- Ensure it loads first
    config = function()
      vim.cmd.colorscheme "northern"
    end
  }
}
