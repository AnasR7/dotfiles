if status is-interactive
    # Commands to run in interactive sessions can go here
end

# alias logo-ls
alias ls="logo-ls"

# alias la
alias la="ls -a"
