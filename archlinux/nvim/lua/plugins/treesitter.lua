return {
  {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    config = function()
      local config = require("nvim-treesitter.configs")
      config.setup({
        auto_set_filetype = false,
        ignore_install = { "telekasten" },
        highlight = {
          enable = true,
          disable = { "telekasten" }
        },
        auto_install = true,
        indent = { enable = true },
      })
    end
  }
}
