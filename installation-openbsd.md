##################################################
#   WELCOME TO INSTALLATION SECTION OF OPENBSD   #
##################################################


# First step
  - login with root
  - download and install fish shell & neovim
    - `pkg_add fish neovim`
  - edit shells in `/etc/shells` & add fish shell in last line 
  - cp doas.conf in /etc/examples/doas.conf to /etc/
  - edit doas.conf, add permit user as root
  - exit
  

# Step 2  
  - login as user
  - check doas if work
  - doas sysctl kern.audio.record=1
  - doas echo kern.audio.record=1 >> /etc/sysctl.conf
  - doas rcctl enable sndiod
  - doas rcctl set sndiod flags -f rsnd/0 -F rsnd/1
  - doas rcctl restart sndiod
  - doas rcctl reload sndiod
  - install stow & git `doas pkg_add stow git`
  - clone dotfiles in user home
    - `git clone https://gitlab.com/AnasR7/dotfiles`
  - install all apps needed
    - `doas pkg_add iosevka chromium rofi arandr nitrogen polybar picom bspwm`
  - open dotfiles and stow dotfiles in `/openbsd/stow_home` & `opensd/stow_root`
