function fish_prompt --description 'Write out the prompt'
    set -l laststatus $status

    set -l git_info
    if set -l git_branch (command git symbolic-ref HEAD 2>/dev/null | string replace refs/heads/ '')
        set git_branch (set_color -o blue)"$git_branch"
        set -l git_status
        if not command git diff-index --quiet HEAD --
            if set -l count (command git rev-list --count --left-right $upstream...HEAD 2>/dev/null)
                echo $count | read -l ahead behind
                if test "$ahead" -gt 0
                    set git_status "$git_status"(set_color red)⬆
                end
                if test "$behind" -gt 0
                    set git_status "$git_status"(set_color red)⬇
                end
            end
            for i in (git status --porcelain | string sub -l 2 | sort | uniq)
                switch $i
                    case "."
                        set git_status "$git_status"(set_color green)"✚ "
                    case " D"
                        set git_status "$git_status"(set_color red)"✖ "
                    case "*M*"
                        set git_status "$git_status"(set_color green)"✱ "
                    case "*R*"
                        set git_status "$git_status"(set_color purple)"➜ "
                    case "*U*"
                        set git_status "$git_status"(set_color brown)"═"
                    case "??"
                        set git_status "$git_status"(set_color red)"≠"
                end
            end
        else
            set git_status (set_color green):
        end
        set git_info "( $git_status$git_branch"(set_color white)")"
    end

    # Disable PWD shortening by default.
    set -q fish_prompt_pwd_dir_length
    or set -lx fish_prompt_pwd_dir_length 0

    # prompt_login
    # printf (set_color yellow)"$USER"(set_color red)" "(set_color green)""(set_color blue)""(set_color normal)"[ "(set_color blue)" "(set_color normal)" ]"
    printf (set_color yellow)"AnasR7"(set_color red)" "(set_color green)""(set_color blue)""(set_color normal)"[ "(set_color blue)" "(set_color normal)" ]"
    echo -n (set_color white)":"
    # PWD
    set_color $fish_color_cwd
    echo -n (prompt_pwd) $git_info
    set_color normal
    
    if test $laststatus -eq 0
        printf "\n 契" (set_color white) (set_color normal)
    else
        printf "\n 契 " (set_color -o red) (set_color normal)
    end
end
