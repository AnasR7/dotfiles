local map = vim.keymap.set
local buf_lsp = vim.lsp.buf

map("n", "<leader>l", "<cmd> Lazy sync <CR>")
map("n", "<leader>ff", "<cmd> Telescope find_files <CR>")
map("n", "<leader>fg", "<cmd> Telescope live_grep <CR>")
map("n", "<leader><leader>", "<cmd> Telescope oldfiles <CR>")
map("n", "<leader>t", "<cmd> Neotree filesystem reveal left<CR>")
map("n", "<leader>tf", "<cmd> Neotree buffers reveal float<CR>")
map("n", "K", buf_lsp.hover, {})
map("n", "<leader>gd", buf_lsp.definition, {})
map("n", "<leader>gr", buf_lsp.references, {})
map("n", "<leader>ca", buf_lsp.code_action, {})
map("n", "<leader>gf", buf_lsp.format, {})
