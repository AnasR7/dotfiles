* add stow_home

        cd stow_home
        stow -t ~ *
* add stow_root

        cd stow_root
        sudo stow [Apps]

* add in bashrc

        # START X DISPLAY GUI
        if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
        startx
        fi
        
        exec fish

* add chaotic aur

        pacman-key --recv-key FBA220DFC880C036 --keyserver keyserver.ubuntu.com
        pacman-key --lsign-key FBA220DFC880C036
        pacman -U 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'
