# DISPLAY POLYBAR
# DISPLAY1="$(xrandr -q | grep 'eDP' | cut -d ' ' -f1)"
# [[ ! -z "$DISPLAY1" ]] && MONITOR="$DISPLAY1" polybar -q top -c "$DIR"/config.ini &
# [[ ! -z "$DISPLAY1" ]] && MONITOR="$DISPLAY1" polybar -q bottom -c "$DIR"/config.ini &
# DISPLAY2="$(xrandr -q | grep 'HDMI-A-0' | cut -d ' ' -f1)"
# [[ ! -z "$DISPLAY2" ]] && MONITOR="$DISPLAY2" polybar -q top -c "$DIR"/config.ini &
# [[ ! -z "$DISPLAY2" ]] && MONITOR="$DISPLAY2" polybar -q bottom -c "$DIR"/config.ini &

# Changing "ls" to "exa"
alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing
alias l.='exa -a | egrep "^\."'

# alias python="python2"
# Create Alias for python3 Pip3
alias pip3='python3 -m pip'

# Create Alias for python3 Pipenv
alias pipenv='python3 -m pipenv'

# Neofetch
# neofetch --ascii_distro artix_small

# Fish-pipenv
set pipenv_fish_fancy yes
