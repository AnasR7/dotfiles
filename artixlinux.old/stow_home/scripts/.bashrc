#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# alias ls='ls --color=auto'
# PS1='[\u@\h \W]\$ '

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# START X DISPLAY GUI
if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
startx
fi


exec fish

DISPLAY1="$(xrandr -q | grep 'eDP' | cut -d ' ' -f1)"
[[ ! -z "$DISPLAY1" ]] && MONITOR="$DISPLAY1" polybar -q top -c "$DIR"/config.ini &
[[ ! -z "$DISPLAY1" ]] && MONITOR="$DISPLAY1" polybar -q bottom -c "$DIR"/config.ini &
DISPLAY2="$(xrandr -q | grep 'HDMI-A-0' | cut -d ' ' -f1)"
[[ ! -z "$DISPLAY2" ]] && MONITOR="$DISPLAY2" polybar -q top -c "$DIR"/config.ini &
[[ ! -z "$DISPLAY2" ]] && MONITOR="$DISPLAY2" polybar -q bottom -c "$DIR"/config.ini &
